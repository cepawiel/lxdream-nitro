/* config.h.in.  Generated from configure.in by autoheader.  */
#undef HAVE_CATGETS
#undef HAVE_GETTEXT
#undef GETTEXT_PACKAGE
#undef HAVE_LC_MESSAGES
#undef HAVE_STPCPY
#undef HAVE_LIBSM

/* Building on an apple platform. Things are different... */
#if @is_apple@
#define APPLE_BUILD 1
#endif
/* CPP to use for build tools */
#define BUILD_CPP_PROG "@cpp_program@"

/* Enable dynamic plugin support */
#define BUILD_PLUGINS @build_plugins@

/* Sed to use for build tools */
#define BUILD_SED_PROG "@sed_program@"

/* always defined to indicate that i18n is enabled */
#define ENABLE_NLS 1

/* Enable SH4 statistics */
#undef ENABLE_SH4STATS

/* Enable IO tracing */
#undef ENABLE_TRACE_IO

/* Enable watchpoints */
#undef ENABLE_WATCH

/* Forcibly inline code */
#define FORCEINLINE __attribute__((always_inline))

/* translation domain */
#undef GETTEXT_PACKAGE

/* Have alsa support */
#if @has_alsa@
#define HAVE_ALSA 1
#endif

/* Define to 1 if you have the `bind_textdomain_codeset' function. */
#undef HAVE_BIND_TEXTDOMAIN_CODESET

/* Have Cocoa framework */
#if @has_cocoa@
#define HAVE_COCOA 1
#endif
/* Have Apple CoreAudio support */
#if @has_core_audio@
#define HAVE_CORE_AUDIO 1
#endif

/* Define to 1 if you have the `dcgettext' function. */
#undef HAVE_DCGETTEXT

/* Have esound support */
#if @has_esound@
#define HAVE_ESOUND 1
#endif

/* Have exception stack-frame information */
#define HAVE_EXCEPTIONS 1

/* Use fast register-passing calling conventions */
#define HAVE_FASTCALL 1

/* Define if we have a working builtin frame_address */
#define HAVE_FRAME_ADDRESS 1

/* Define if the GNU gettext() function is already present or preinstalled. */
#undef HAVE_GETTEXT

/* Using GLESv2 */
#if @has_gles2@
#define HAVE_GLES2 1
#endif

/* Have GLX support */
#if @has_glx@
#define HAVE_GLX 1
#endif

/* Have GTK libraries */
#if @has_gtk@
#define HAVE_GTK 1
#endif

/* Building with GTK+Cocoa */
#undef HAVE_GTK_OSX

/* Building with GTK+X11 */
#define HAVE_GTK_X11 1

/* Define to 1 if you have the <inttypes.h> header file. */
#undef HAVE_INTTYPES_H

/* Define if your <locale.h> file defines LC_MESSAGES. */
#undef HAVE_LC_MESSAGES

/* Define to 1 if you have the `OSMesa' library (-lOSMesa). */
#if @has_osmesa@
#define HAVE_LIBOSMESA 1
#endif

/* Define to 1 if you have the `z' library (-lz). */
#define HAVE_LIBZ 1

#if @is_linux@
#define HAVE_LINUX_CDROM 1
#define HAVE_LINUX_JOYSTICK 1
#endif

/* Have LIRC support */
#undef HAVE_LIRC

/* Define to 1 if you have the <locale.h> header file. */
#undef HAVE_LOCALE_H

/* Define to 1 if you have the <memory.h> header file. */
#define HAVE_MEMORY_H 1

/* Have NSOpenGL support */
#undef HAVE_NSGL

/* Have Color Clamp */
#define HAVE_OPENGL_CLAMP_COLOR 1

/* Have glClearDepthf function */
#define HAVE_OPENGL_CLEAR_DEPTHF 1

/* Have glDrawBuffer function */
#define HAVE_OPENGL_DRAW_BUFFER 1

/* Have 2.0 framebuffer_object support */
#define HAVE_OPENGL_FBO 1

/* Have EXT_framebuffer_object support */
#define HAVE_OPENGL_FBO_EXT 1

/* Have OpenGL fixed-functionality */
#define HAVE_OPENGL_FIXEDFUNC 1

/* Have 2.0 shader support */
#define HAVE_OPENGL_SHADER 1

/* Have ARB shader support */
#define HAVE_OPENGL_SHADER_ARB 1

/* Have glAreTexturesResident function */
#define HAVE_OPENGL_TEX_RESIDENT 1

/* Building with the OSMesa video driver */
#if @has_osmesa@
#define HAVE_OSMESA 1
#endif

/* Have pulseaudio support */
#if @has_pulse@
#define HAVE_PULSE 1
#endif

/* Have SDL support */
#if @has_sdl@
#define HAVE_SDL 1
#endif

/* Define to 1 if you have the <stdint.h> header file. */
#define HAVE_STDINT_H 1

/* Define to 1 if you have the <stdlib.h> header file. */
#define HAVE_STDLIB_H 1

/* Define to 1 if you have the <strings.h> header file. */
#define HAVE_STRINGS_H 1

/* Define to 1 if you have the <string.h> header file. */
#define HAVE_STRING_H 1

/* Define to 1 if you have the <sys/stat.h> header file. */
#if @has_sys_stat_h@
#define HAVE_SYS_STAT_H 1
#endif

/* Define to 1 if you have the <sys/types.h> header file. */
#if @has_sys_types_h@
#define HAVE_SYS_TYPES_H 1
#endif

/* Define to 1 if you have the <unistd.h> header file. */
#if @has_unistd_h@
#define HAVE_UNISTD_H 1
#endif

/* Generating a bundled application */
#undef OSX_BUNDLE

/* Name of package */
#define PACKAGE "@package@"

/* Define to the address where bug reports for this package should be sent. */
#undef PACKAGE_BUGREPORT

/* Define to the full name of this package. */
#define PACKAGE_NAME "@package@"

/* Define to the full name and version of this package. */
#define PACKAGE_STRING "@package_string@"

/* Define to the one symbol short name of this package. */
#define PACKAGE_TARNAME "@package_tarname@"

/* Define to the version of this package. */
#define PACKAGE_VERSION "@package_version@"

/* SH4 Translator to use (if any) */
#undef SH4_TRANSLATOR

/* The size of `void *', as computed by sizeof. */
#define SIZEOF_VOID_P @sizeof_voidp@

/* Define to 1 if you have the ANSI C header files. */
#define STDC_HEADERS 1

/* Version number of package */
#define VERSION "@package_version@"

#define PREFIX "@prefix@"
#define SYSCONFDIR PREFIX"/etc"
#define LIBEXECDIR PREFIX"/libexec"

#define PACKAGE_CONF_DIR "SYSCONFDIR"
#define PACKAGE_LOCALE_DIR PREFIX"/share/locale"
#define PACKAGE_PLUGIN_DIR LIBEXECDIR
#define PACKAGE_DATA_DIR PREFIX"/share"
