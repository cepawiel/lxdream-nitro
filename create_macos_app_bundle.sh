#!/bin/bash

SOURCE_ROOT=${MESON_SOURCE_ROOT}
BUILD_ROOT=${MESON_BUILD_ROOT}
BUNDLE=${BUILD_ROOT}/lxdream-nitro.app

echo '--- Building Mac Bundle ---'
rm -rf ${BUNDLE}
mkdir -p ${BUNDLE}/Contents/MacOS
mkdir -p ${BUNDLE}/Contents/Resources
mkdir -p ${BUNDLE}/Contents/Plugins
cp ${BUILD_ROOT}/Info.plist ${BUNDLE}/Contents
cp ${SOURCE_ROOT}/lxdreamrc ${BUNDLE}/Contents/Resources
cp ${BUILD_ROOT}/lxdream-nitro ${BUNDLE}/Contents/MacOS
# TODO: Build the lxdream_dummy.dylib if it's actually necessary
#cp -R ${SOURCE_ROOT}/src/*.dylib ${BUNDLE}/Contents/Plugins
cp -R ${SOURCE_ROOT}/pixmaps/* ${BUNDLE}/Contents/Resources
${SOURCE_ROOT}/bundlelibs.pl ${BUNDLE}/Contents/MacOS/lxdream-nitro ${BUNDLE}/Contents/Frameworks
# TODO: Process and include language files
# for cat in $(CATALOGS); do \
#     catname=`basename "$$cat"`; \
#     catname=`echo $$catname|sed -e 's/$(CATOBJEXT)$$//'`; \
#     mkdir -p "${BUNDLE}/Contents/Resources/$$catname/LC_MESSAGES"; \
#     cp "$(top_SOURCE_ROOT)/po/$$cat" "${BUNDLE}/Contents/Resources/$$catname/LC_MESSAGES/lxdream$(INSTOBJEXT)"; \
# done
echo '--- Done ---'